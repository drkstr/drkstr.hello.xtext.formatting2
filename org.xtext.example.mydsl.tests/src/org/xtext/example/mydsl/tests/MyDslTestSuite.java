package org.xtext.example.mydsl.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({MyDslParsingTest.class, MyDslFormatterTest.class})
public class MyDslTestSuite {

}
