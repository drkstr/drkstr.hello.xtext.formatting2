package org.xtext.example.mydsl.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(MyDslInjectorProvider)
class MyDslFormatterTest {

	@Inject extension FormatterTestHelper

	@Test
	def void indentSteps() {
		assertFormatted[
			toBeFormatted = '''
	            Scenario: Hello, World!
	            * The quick brown fox
	            * Jumps over the lazy dog
			'''
			expectation = '''
	            Scenario: Hello, World!
	            	* The quick brown fox
	            	* Jumps over the lazy dog
			'''
		]
	}
}
